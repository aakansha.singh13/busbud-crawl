# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class BusbudItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    places = scrapy.Field()
    reference = scrapy.Field()
    source = scrapy.Field()
    dest = scrapy.Field()
    SD_geo_hash = scrapy.Field()
    # pass
