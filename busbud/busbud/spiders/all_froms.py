import scrapy
import pandas as pd


class RoutesSpider(scrapy.Spider):
    name = 'source'
    # start_urls = ["https://www.busbud.com/en-gb"]
    start_urls = ['https://www.busbud.com/en-gb/sitemap/wr/0']
    base_url = "https://www.busbud.com"

    # All countries alphabetically
    def parse(self, response):
        all_countries = response.css("li.suggestion").xpath(".//a/@href").extract()
        for country in all_countries:
            country_code = country.split("/")[-1]
            yield scrapy.Request(self.base_url + country, callback=self.parse_country_page,
                                 meta={'country_code': country_code})

    # A particular country and its source cities
    def parse_country_page(self, response):
        country_code = response.meta.get('country_code')
        country_name = response.css("h1::text").extract_first().replace(" bus routes", "")
        places_href_list = response.css("li.suggestion").xpath(".//a/@href").extract()
        places_names_list = response.css("li.suggestion").xpath(".//a/text()").extract()
        df = pd.DataFrame()
        df['Places'] = places_names_list
        df['Places'] = df['Places'].apply(
            lambda x: str(x).split(" in ")[1] if " in " in str(x) else str(x).split(" from ")[1])
        df['Reference'] = places_href_list
        for index, row in df.iterrows():
            # yield scrapy.Request(self.base_url + row['Reference'], callback=self.parse_source_dest_page,
            #                      meta={'country_code': country_code, 'country_name': country_name})

            yield {'country_name': country_name, 'country_code': country_code,
                   'all_references': row['Reference'], 'place': row['Places']}
