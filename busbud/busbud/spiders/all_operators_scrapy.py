import scrapy
import pandas as pd


class OperatorSpider(scrapy.Spider):
    name = 'operators'
    start_urls = ["https://www.busbud.com"]
    base_url = "https://www.busbud.com"

    def parse(self, response):
        # df = pd.read_csv("/Users/aakansha.s/Documents/Projects/Scrapy/busbud/all_source_destination.csv")
        # df = df.head(20)
        # df.loc[df['country_name'] == 'Namibia', "country_code"] = "NA"
        # df['geo_hash_value'] = df["reference"].apply(lambda x: x.split("/")[-1])
        # for index, row in df.iterrows():
        #     new_url = self.base_url + "/en-gb/bus-schedules-results/" + row['geo_hash_value'].split("-")[0] + "/" \
        #               + row['geo_hash_value'].split("-")[1] + "?outbound_date=" + "2020-08-23&adults=1"
        #     print(new_url)
        #     yield scrapy.Request(new_url, callback=self.parse_country_data, meta={'country_name': row['country_name'],
        #                                                                           'country_code': row['country_code'],
        #                                                                           'geo_hash_value': row[
        #                                                                               'geo_hash_value'],
        #                                                                           'source': row['source'],
        #                                                                           'destination': row['destination']})

        print(response)

    def parse_country_data(self, response):
        country_name = response.meta().get("country_name")
        country_code = response.meta().get("country_code")
        geo_hash_value = response.meta().get("geo_hash_value")
        source = response.meta().get("source")
        destination = response.meta().get("destination")
        all_operators = response.css("div.departure-card::attr('data-operator')").getall()
        all_operators = set(all_operators)
        if len(all_operators) == 0:
            all_operators = ""
        yield {'country_name': country_name, 'country_code': country_code, 'geo_hash_value': geo_hash_value,
               'source': source, 'destination': destination, 'operators': all_operators}
