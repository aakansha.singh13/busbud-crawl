import scrapy
import pandas as pd


class RoutesSpider(scrapy.Spider):
    name = 'routes'
    # start_urls = ["https://www.busbud.com/en-gb"]
    start_urls = ['https://www.busbud.com/en-gb/sitemap/wr/0']
    base_url = "https://www.busbud.com"

    # # All Bus Routes Page in home page
    # def parse(self, response):
    #     all_routes_href = response.css("footer#main-footer").xpath(".//div//div//ul//li//a/@href")[1].extract()
    #     yield scrapy.Request(response.urljoin(all_routes_href), callback=self.parse_all_routes_page)

    # All countries alphabetically
    def parse(self, response):
        all_countries = response.css("li.suggestion").xpath(".//a/@href").extract()
        # for country in all_countries:
        #     country_code = country.split("/")[-1]
        #     yield scrapy.Request(self.base_url + country, callback=self.parse_country_page,
        #                          meta={'country_code': country_code})

        length_all_countries = len(all_countries)
        print(length_all_countries)
        for i in range(30, 40):
            country = all_countries[i]
            country_code = country.split("/")[-1]
            yield scrapy.Request(self.base_url + country, callback=self.parse_country_page,
                                 meta={'country_code': country_code})

    # A particular country and its source cities
    def parse_country_page(self, response):
        country_code = response.meta.get('country_code')
        country_name = response.css("h1::text").extract_first().replace(" bus routes", "")
        places_href_list = response.css("li.suggestion").xpath(".//a/@href").extract()
        places_names_list = response.css("li.suggestion").xpath(".//a/text()").extract()
        df = pd.DataFrame()
        df['Places'] = places_names_list
        df['Places'] = df['Places'].apply(
            lambda x: str(x).split(" in ")[1] if " in " in str(x) else str(x).split(" from ")[1])
        df['Reference'] = places_href_list
        df['geo_hash_value'] = df['Reference'].apply(lambda x: str(x).split("/")[-1])
        for index, row in df.iterrows():
            yield scrapy.Request(self.base_url + row['Reference'], callback=self.parse_source_dest_page,
                                 meta={'country_code': country_code, 'country_name': country_name})

    # A particular city of a particular country and all the bus routes from that city to some other city
    def parse_source_dest_page(self, response):
        country_name = response.meta.get("country_name")
        country_code = response.meta.get("country_code")
        places_href_list = response.css("li.suggestion").xpath(".//a/@href").extract()
        places_names_list = response.css("li.suggestion").xpath(".//a/text()").extract()
        df = pd.DataFrame()
        df['Reference'] = places_href_list
        df['Places'] = places_names_list
        df[['source', 'dest']] = df['Places'].apply(lambda x: pd.Series(str(x).split(" from ")[1].split(" to ")))
        df['geo_hash_value'] = df['Reference'].apply(lambda x: str(x).split("/")[-1])
        df.drop(columns={'Places','Reference'}, inplace=True)
        for index, row in df.iterrows():
            new_url = "/en-gb/bus-schedules-results/" + \
                      row['geo_hash_value'].split("-")[0] + "/" + \
                      row['geo_hash_value'].split("-")[1] + "?outbound_date=2020-08-25&adults=1"
            yield scrapy.Request(self.base_url + new_url, callback=self.parse_operators,
                                 meta={'country_code':country_code, 'country_name': country_name,
                                       'source': row['source'], 'dest': row['dest'],
                                       'geo_hash_value': row['geo_hash_value']})

    # All the operators in the above selected source destination
    def parse_operators(self, response):
        country_name = response.meta.get("country_name")
        country_code = response.meta.get("country_code")
        geo_hash_value = response.meta.get("geo_hash_value")
        source = response.meta.get("source")
        destination = response.meta.get("dest")
        all_operators = response.css("div.departure-card::attr('data-operator')").getall()
        all_operators = set(all_operators)
        if len(all_operators) == 0:
            all_operators = ""
        yield {'country_name': country_name, 'country_code': country_code, 'geo_hash_value': geo_hash_value,
               'source': source, 'destination': destination, 'operators': all_operators}
