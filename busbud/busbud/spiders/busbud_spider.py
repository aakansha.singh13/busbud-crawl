import scrapy
import pandas as pd
import numpy as np
from ..items import BusbudItem


class BudbusSpider(scrapy.Spider):
    name = 'bud'
    start_urls = ["https://www.busbud.com"]

    def parse(self, response):
        divs = response.xpath("//div[@class='my-1']")
        place_list = []
        href_list = []
        df = pd.DataFrame()
        for d in divs.xpath(".//div[@class='row']"):
            columns = d.xpath(".//div[@class='column medium-4 end']")
            for c in columns.xpath(".//ul[@class='list-unstyled']"):
                place_list = place_list + c.xpath(".//a/text()").extract()
                href_list = href_list + c.xpath(".//a/@href").extract()
            break

        # items['places'] = place_list
        # items['reference'] = href_list
        # items['source'] = [x.split(" from ")[1].split(' to ')[0] for x in place_list]
        # items['dest'] = [x.split(" from ")[1].split(' to ')[1] for x in place_list]
        # items['SD_geo_hash'] = [x.split("/")[4] for x in href_list]
        # for geo_hash in items['SD_geo_hash']:
        #     new_url = "en-gb/bus-schedules-results/" + geo_hash.split("-")[0] + "/" + \
        #               geo_hash.split("-")[1] + "?outbound_date=2020-08-25&adults=1"
        #     yield scrapy.Request(response.urljoin(new_url), callback=self.parse_sd_page,
        #                          meta={'geo_hash_value': geo_hash})

        df['places'] = place_list
        df['reference'] = href_list
        df['source'] = df['places'].apply(lambda x: x.split(" from ")[1].split(' to ')[0])
        df['dest'] = df['places'].apply(lambda x: x.split(" from ")[1].split(' to ')[1])
        df['SD_geo_hash'] = df['reference'].apply(lambda x: x.split("/")[4])

        for index, row in df.iterrows():
            new_url = "en-gb/bus-schedules-results/" + row['SD_geo_hash'].split("-")[0] + "/" + \
                      row['SD_geo_hash'].split("-")[1] + "?outbound_date=2020-08-25&adults=1"
            yield scrapy.Request(response.urljoin(new_url), callback=self.parse_sd_page,
                                 meta={'geo_hash_value': row['SD_geo_hash'],
                                       'source': row['source'], 'dest': row['dest']})

    def parse_sd_page(self, response):
        all_operators = response.css("div.departure-card::attr('data-operator')").getall()
        all_operators = set(all_operators)
        if len(all_operators) == 0:
            all_operators = ""
        yield {'geo_hash_value': response.meta.get('geo_hash_value'), 'operators': all_operators,
               'source': response.meta.get('source'), 'dest': response.meta.get('dest')}