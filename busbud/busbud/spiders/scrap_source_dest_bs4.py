import requests
import soup as soup
from bs4 import BeautifulSoup
import pandas as pd
from multiprocessing import Pool
import numpy as np
import uuid

base_url = "https://www.busbud.com"

df = pd.read_csv("/Users/aakansha.s/Documents/Projects/busbud-crawl/busbud/all_sources.csv")

with open('/Users/aakansha.s/Documents/Projects/Scrapy/busbud-crawl/busbud/http_proxies.txt', 'r') as data:
    lines = data.readlines()

proxies = {'http': []}

for line in lines:
    ip_port = line.split(':')
    ip, port = (ip_port[0], ip_port[1]) if len(ip_port) > 1 else (ip_port[0], 80)
    scheme = 'http'
    proxies[scheme].append(scheme + '://' + ip + ':' + port)


def scrape(url):
    country_name = url.split("::")[1]
    country_code = url.split("::")[2]
    r = requests.get(base_url + url.split("::")[0], proxies=proxies)
    soup = BeautifulSoup(r.content, 'html.parser')
    result = soup.find_all("li", class_="suggestion")
    df = pd.DataFrame()
    for r in result:
        reference = r.find("a")['href']
        place = r.find("a").text
        source = place.split(" from ")[1].split(" to ")[0]
        dest = place.split(" from ")[1].split(" to ")[1]
        print(place + "  " + country_name)
        df = df.append({'country_name': country_name, 'country_code': country_code,
                        'reference': reference, 'source': source, 'destination': dest}, ignore_index=True)

    df.to_csv("/Users/aakansha.s/Desktop/AllCountries/dataframe_" + str(uuid.uuid1()) + ".csv", index=False)


p = Pool(10)
df = pd.read_csv("/Users/aakansha.s/Documents/Projects/busbud-crawl/busbud/all_sources.csv")
df.loc[df['country_name']=='Namibia', "country_code"] = "NA"
all_urls = list(df['all_references'] + "::" + df['country_name'] + "::" + df['country_code'])
p.map(scrape, all_urls)
p.terminate()
p.join()
